package cvrk.matija.algorithms;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

class StringSearchRabinKarpTest {

  @Test
  void should_match_pattern_properly() {
    Assertions
      .assertThat(StringSearchRabinKarp.match("AABA", "AABAACAADAABAABA", 101))
      .isEqualTo(new int[] { 0, 9, 12 });

    Assertions
      .assertThat(StringSearchRabinKarp.match("AAA", "AAAAAAAAAA", 101))
      .isEqualTo(IntStream.range(0, 8).toArray());

    Assertions
      .assertThat(StringSearchRabinKarp.match("home", " home sweet home", 101))
      .isEqualTo(new int[] { 1, 12 });
  }
}
