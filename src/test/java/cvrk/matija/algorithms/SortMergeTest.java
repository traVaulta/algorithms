package cvrk.matija.algorithms;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SortMergeTest {

  @Test
  void should_sort_values_properly() {
    int[] values = new int[] { 4, 5, 1, 3, 4, 2 };
    SortMerge.sort(values, 0, values.length - 1);
    Assertions.assertThat(values).isSorted();
  }
}
