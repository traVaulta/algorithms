package cvrk.matija.algorithms;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class StringSearchKnuthMorrisPrattTest {

  @Test
  void should_match_pattern_properly() {
    Assertions
      .assertThat(StringSearchKnuthMorrisPratt.match("AABA", "AABAACAADAABAABA"))
      .isEqualTo(new int[] { 0, 9, 12 });
  }
}
