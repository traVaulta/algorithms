package cvrk.matija.algorithms;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SortInsertionTest {

  @Test
  void should_sort_properly() {
    int[] values = new int[] { 4, 5, 1, 3, 4, 2 };
    SortInsertion.sort(values);
    Assertions.assertThat(values).isSorted();
  }
}
