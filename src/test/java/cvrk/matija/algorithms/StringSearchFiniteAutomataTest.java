package cvrk.matija.algorithms;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class StringSearchFiniteAutomataTest {

  @Test
  void should_match_pattern_properly() {
    Assertions
      .assertThat(StringSearchFiniteAutomata.match("AABA", "AABAACAADAABAABA"))
      .isEqualTo(new int[] { 0, 9, 12 });

    Assertions
      .assertThat(StringSearchFiniteAutomata.match("AAA", "AAAAAAAAAA"))
      .isEqualTo(IntStream.range(0, 8).toArray());

    Assertions
      .assertThat(StringSearchFiniteAutomata.match("home", " home sweet home"))
      .isEqualTo(new int[] { 1, 12 });
  }
}
