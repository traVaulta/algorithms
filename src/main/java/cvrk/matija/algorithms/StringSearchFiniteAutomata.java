package cvrk.matija.algorithms;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class StringSearchFiniteAutomata {
  private static final int MAX_CHAR_COUNT = Math.abs(2 * Byte.MIN_VALUE);

  private StringSearchFiniteAutomata() {}

  public static int[] match(String pattern, String text) {
    char[] chars = pattern.toCharArray();
    int m = pattern.length();
    int n = text.length();
    Queue<Integer> queue = new ConcurrentLinkedQueue<>();
    int[][] powerSet = new int[m + 1][MAX_CHAR_COUNT];

    computeFAPowerSet(chars, m, powerSet);

    int i, state = 0;
    for (i = 0; i < n; i++) {
      state = powerSet[state][text.charAt(i)];
      if (state == m) {
        queue.add(i - m + 1);
      }
    }

    return queue.stream().mapToInt(val -> val).toArray();
  }

  private static void computeFAPowerSet(char[] pattern, int m, int[][] powerSet) {
    int state, current;
    for (state = 0; state <= m; state++) {
      for (current = 0; current < MAX_CHAR_COUNT; ++current) {
        powerSet[state][current] = getNextState(pattern, m, state, current);
      }
    }
  }

  private static int getNextState(char[] pattern, int m, int state, int current) {
    if (state < m && current == pattern[state]) {
      return state + 1;
    }

    int following, target;
    for (following = state; following > 0; following--) {
      if (pattern[following - 1] == current) {
        for (target = 0; target < following - 1; target++) {
          if (pattern[target] != pattern[state - following + 1 + target]) {
            break;
          }
        }
        if (target == following - 1) {
          return following;
        }
      }
    }

    return 0;
  }
}
