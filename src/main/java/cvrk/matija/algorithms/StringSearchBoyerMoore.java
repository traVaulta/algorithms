package cvrk.matija.algorithms;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class StringSearchBoyerMoore {
  static int MAX_CHAR_COUNT = 256;

  private StringSearchBoyerMoore() {}

  public static int[] match(String pattern, String text) {
    Queue<Integer> queue = new ConcurrentLinkedQueue<>();
    int m = pattern.length();
    int n = text.length();
    int[] occurrences = new int[MAX_CHAR_COUNT];

    badHeuristics(pattern, m, occurrences);

    int s = 0;
    while (s <= (n - m)) {
      int j = m - 1;
      while (j >= 0 && pattern.charAt(j) == text.charAt(s + j)) {
        j--;
      }

      if (j < 0) {
        queue.add(s);
        s += (s + m < n) ? m - occurrences[text.charAt(s + m)] : 1;
      } else {
        s += Math.max(1, j - occurrences[text.charAt(s + j)]);
      }
    }

    return queue.stream().mapToInt(i -> i).toArray();
  }

  private static void badHeuristics(String pattern, int m, int[] occurrences) {
    int i;

    for (i = 0; i < MAX_CHAR_COUNT; i++) {
      occurrences[i] = -1;
    }

    for (i = 0; i < m; i++) {
      occurrences[pattern.charAt(i)] = i;
    }
  }
}
