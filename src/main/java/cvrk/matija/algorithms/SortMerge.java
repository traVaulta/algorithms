package cvrk.matija.algorithms;

public final class SortMerge {
  private SortMerge() {}

  public static void sort(int[] values, int left, int right) {
    if (left < right) {
      int current = left + (right - left) / 2;

      sort(values, left, current);
      sort(values, current + 1, right);

      merge(values, left, current, right);
    }
  }

  private static void merge(int[] values, int left, int current, int right) {
    int lenLeft = current - left + 1;
    int lenRight = right - current;

    int[] subLeft = new int[lenLeft];
    int[] subRight = new int[lenRight];

    for (int i = 0; i < lenLeft; i++) {
      subLeft[i] = values[i];
    }
    for (int i = 0; i < lenRight; i++) {
      subRight[i] = values[i + current + 1];
    }

    int i = 0, j = 0;

    int key = left;
    while (i < lenLeft && j < lenRight) {
      if (subLeft[i] <= subRight[j]) {
        values[key] = subLeft[i];
        i++;
      }
      else {
        values[key] = subRight[j];
        j++;
      }
      key++;
    }
    while (i < lenLeft) {
      values[key] = subLeft[i];
      i++;
      key++;
    }
    while (j < lenRight) {
      values[key] = subRight[j];
      j++;
      key++;
    }
  }
}
