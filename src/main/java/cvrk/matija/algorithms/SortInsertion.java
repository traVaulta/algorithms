package cvrk.matija.algorithms;

public final class SortInsertion {
  private SortInsertion() {}

  public static void sort(int[] values) {
    int i;
    for (i = 1; i < values.length; i++) {
      int current = values[i];

      int j = i - 1;
      while (j >= 0 && current < values[j]) {
        values[j + 1] = values[j];
        j--;
      }
      values[j + 1] = current;
    }
  }
}
