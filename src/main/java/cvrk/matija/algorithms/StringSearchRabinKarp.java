package cvrk.matija.algorithms;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class StringSearchRabinKarp {
  private static final int MAX_CHAR_COUNT = 256;

  private StringSearchRabinKarp() {}

  public static int[] match(String pattern, String text, int prime) {
    int m = pattern.length();
    int n = text.length();
    int i, j;
    Queue<Integer> queue = new ConcurrentLinkedQueue<>();

    int hash = 1, textHash = 0, patternHash = 0;

    for (i = 0; i < m - 1; i++) {
      hash = (hash * MAX_CHAR_COUNT) % prime;
    }

    for (i = 0; i < m; i++) {
      patternHash = (MAX_CHAR_COUNT * patternHash + pattern.charAt(i)) % prime;
      textHash = (MAX_CHAR_COUNT * textHash + text.charAt(i)) % prime;
    }

    for (i = 0; i <= n - m; i++) {
      if (patternHash == textHash) {
        for (j = 0; j < m; j++) {
          if (text.charAt(i + j) != pattern.charAt(j)) {
            break;
          }
        }

        if (j == m) {
          queue.add(i);
        }
      }

      if (i < n - m) {
        textHash = (MAX_CHAR_COUNT * (textHash - text.charAt(i) * hash) + text.charAt(i + m)) % prime;

        if (textHash < 0) {
          textHash += prime;
        }
      }
    }

    return queue.stream().mapToInt(val -> val).toArray();
  }
}
