package cvrk.matija.algorithms;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class StringSearchKnuthMorrisPratt {
  private StringSearchKnuthMorrisPratt() {}

  public static int[] match(String pattern, String text) {
    int m = pattern.length();
    int n = text.length();

    int[] lps = new int[m];
    int j = 0;
    Queue<Integer> queue = new ConcurrentLinkedQueue<>();

    preProcessLPSArray(pattern, m, lps);

    int i = 0;
    while (i < n) {
      if (pattern.charAt(j) == text.charAt(i)) {
        ++j;
        ++i;
      }
      if (j == m) {
        j = lps[j - 1];
        queue.add(i - m);
      } else if (i < n && pattern.charAt(j) != text.charAt(i)) {
        if (j != 0) {
          j = lps[j - 1];
        } else {
          i = i + 1;
        }
      }
    }

    return queue.stream().mapToInt(v -> v).toArray();
  }

  private static void preProcessLPSArray(String pattern, int m, int[] lps) {
    int lenMaxLPS = 0;
    int i = 1;
    lps[0] = 0;

    while (i < m) {
      if (pattern.charAt(i) == pattern.charAt(lenMaxLPS)) {
        ++lenMaxLPS;
        lps[i] = lenMaxLPS;
        ++i;
      } else {
        if (lenMaxLPS != 0) {
          lenMaxLPS = lps[lenMaxLPS - 1];
        } else {
          lps[i] = lenMaxLPS;
          ++i;
        }
      }
    }
  }
}
