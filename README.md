# Algorithms

A small list of popular algorithms includes:
- sorting
  - insertion sort
  - merge sort
- string pattern matching
  - Knuth, Morris, Pratt
  - Boyer, Moore
  - Rabin, Karp
  - Finite Automata

## Author

- [Matija Čvrk](https://hr.linkedin.com/in/matija-%C4%8Dvrk-1388b3101/)
